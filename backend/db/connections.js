const mysql = require('mysql');

const HOSTNAME = 'localhost';
const USER = 'root';
const PASSWORD = '';
const DATABASE = 'nikunjtest';

var pool = mysql.createPool({
    connectionLimit : 1000,
    connectTimeout  : 60 * 60 * 1000,
    acquireTimeout  : 60 * 60 * 1000,
    timeout         : 60 * 60 * 1000,
    host: HOSTNAME,
    //port: PORT,
    user: USER,
    password: PASSWORD,
    database: DATABASE,
    multipleStatements: true
});

module.exports = pool;
