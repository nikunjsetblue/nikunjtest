var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
const conn = require('../db/connections');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var JWTSECRET = process.env.JWTSECRET;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/admin/login', [
  check('email').exists(),
  check('password').exists()
], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    var { email, password } = req.body
    conn.query('SELECT * FROM `admin` WHERE email = ? and password = ?', [email, crypto.createHash('sha256').update(password, 'utf8').digest('hex')], async(err, resuser) => {
      if (err) {
        res.json({status: 501, message: err});
      } else if (resuser.length > 0) {
        var jwtdata = {
          admin_id: resuser[0].admin_id,
          firstname: resuser[0].firstname,
          lastname: resuser[0].lastname,
          email: resuser[0].email
        }
        var token = jwt.sign({ jwtdata, IsAdminLogin: true }, JWTSECRET, { expiresIn: '10d' });
        res.json({status: 200, message: 'Welcome', data: resuser[0], token: token, IsAdminLogin: true});
      } else {
        res.json({status: 201, message: 'Invalid credentials.'});
      }
    })
  }
});

router.post('/admin/getAdmin',[
  check('admin_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var admin_id = req.body.admin_id;
    conn.query(`SELECT * FROM admin WHERE admin_id = ?`, [admin_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get Data Successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/admin/GetUsers',[
  check('columns').exists(),
  check('start').exists(),
  check('order').exists(),
  check('length').exists()
],(req,res)=>{
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  } else {
    var { columns, order, start, length } = req.body;
    let sql = `SELECT * FROM users ORDER BY ${columns[order[0].column].data} ${order[0].dir} LIMIT ${start},${length};SELECT count(*) as cnt FROM users`;
    conn.query(sql, (error, resdata) => {
      if(error){
        res.json({status: 400, message: error});
      }else{
        if(resdata[0].length > 0){
          res.json({status: 200, message: "Get data successfully", response: resdata[0], TotalRecords:resdata[1][0]});
        }else{
          res.json({status: 400, message: 'No data found'});
        }
      }
    });
  }
});

router.post('/admin/GetParticularUser',[
  check('user_id').exists(),
],function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 501, message: err});
    return
  }else {
    var user_id = req.body.user_id;
    conn.query(`SELECT * FROM users WHERE user_id = ?`, [user_id], (err, results) => {
      if (err) {
        res.json({status: 400, message: err});
      } else {
        if (results.length > 0) {
          res.json({status: 200, message: 'Get data successfully', data: results[0]});
        } else {
          res.json({status: 400, message: 'No data Found'});
        }
      }
    });
  }
});

router.post('/admin/addUser',async function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const err = errors.array().map((err) => ({ field: err.param, message: err.msg }));
    res.json({status: 401, message: err});
    return
  }else {
    let { user_id,fullname,email,mobile } = req.body;
    let data = {
      fullname : fullname,
      email : email,
      mobile : mobile,
      created_at : new Date()
    };
    if (req.files && Object.keys(req.files).length > 0) {
      if (req.files.audiofile !== undefined) {
        let audio = req.files.audiofile;
        audio.name = audio.name.replace(/ /g,"_");
        let filename = Math.floor(Math.random() * 100000) + '-' + audio.name;
        let file_url = './upload/' + filename;
        let file_name = 'upload/' + filename;
        await audio.mv(file_url, function (error) {
          if (error) {
            res.json({status: 400, message: error});
          } else {
            data.audiofile = file_name;
            display()
          }
        });
      }
    }else{
      display()
    }
    async function display() {
      await conn.query(`SELECT * FROM users WHERE user_id = ?`, [user_id], async (err, results) => {
        if (err) {
          res.json({status: 400, message: err});
        } else {
          if (results.length > 0) {
            data.updated_at = new Date()
            await conn.query(`UPDATE users SET ? WHERE user_id = ?`, [data, user_id], (err, results) => {
              if (err) {
                res.json({status: 400, message: err});
              } else {
                if (results.affectedRows === 1) {
                  res.json({status: 200, message: "User update successfully", response: results[0]});
                } else {
                  res.json({status: 400, message: 'No data Found'});
                }
              }
            });
          }else{
            await conn.query(`INSERT INTO users SET ?`, data, (err, results) => {
              if (err) {
                res.json({status: 400, message: err});
              } else {
                if (results.affectedRows === 1) {
                  res.json({status: 200, message: "User created successfully", response: results[0]});
                } else {
                  res.json({status: 400, message: 'No data Found'});
                }
              }
            });
          }
        }
      });
    }
  }
});

module.exports = router;
