import {Component, Inject, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {HttpService} from "../../service/http.service";
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  submitted = false;
  AddForm !: FormGroup;
  constructor(@Inject(DOCUMENT) private document: Document, public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) { }

  ngOnInit(): void {
    this.document.body.classList.add('bg-default');
    this.AddForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,Validators.minLength(6)]]
    });
  }

  get fval() {
    return this.AddForm.controls;
  }

  onSubmit(){
    this.submitted = true;
    if (this.AddForm.invalid) {
      return;
    }
    var data = this.AddForm.value;
    this.http.PostAPI('admin/login', data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.http.token = resdata.token;
        localStorage.setItem('Admin', JSON.stringify(resdata.data));
        localStorage.setItem('AdminToken', resdata.token);
        this.http.IsAdminLogin = true;
        this.toastr.success(resdata.message);
        this.router.navigate(['admin/users']);
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

}
