import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {HttpService} from "../../service/http.service";

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  admin_id = ''
  firstname = ''
  lastname = ''
  constructor(public toastr: ToastrService, private formBuilder: FormBuilder, public router: Router, public http: HttpService) {
    if(localStorage.getItem('Admin')){
      let CurrentAdmin = JSON.parse(<string>localStorage.getItem('Admin'));
      this.admin_id = CurrentAdmin.admin_id;
    }
  }

  ngOnInit(): void {
    this.get_data()
  }

  get_data(){
    var data = {admin_id : this.admin_id};
    this.http.PostAPI('admin/getAdmin', data).then((resdata: any) => {
      if (resdata.status == 200) {
        var json_data = resdata.data;
        this.firstname = json_data.firstname;
        this.lastname = json_data.lastname;
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  logout(){
    this.http.logout()
  }

}
