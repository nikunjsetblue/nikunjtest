import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {environment} from "../../../environments/environment";
import {DOCUMENT} from "@angular/common";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {HttpService} from "../../service/http.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  datatableElement!: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  getlist:any[]=[];
  backend_url = environment.backend_url;
  title = 'Add'
  public show_model = false;
  public show_model1 = false;
  userForm!:FormGroup;
  submitted = false;
  user_id = ''
  audiofile !: File;
  previewaudiofile = ''
  fullname = ''
  email = ''
  mobile = ''
  constructor(@Inject(DOCUMENT) private document: Document,public router: Router, public toastr: ToastrService, public http: HttpService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.document.body.classList.add('bg-default');
    this.datatableInit()
    this.formValidation()
  }

  formValidation(){
    this.userForm = this.formBuilder.group({
      fullname : ['',[Validators.required]],
      email : ['',[Validators.required, Validators.email]],
      audiofile : ['',[Validators.required]],
      mobile : ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]]
    });
  }

  paginate:any =  {
    oPaginate: {
      sPrevious: '<i class="fa fa-angle-left "></i>',
      sFirst: '<i class="fa fa-angle-double-left"></i>',
      sNext: '<i class="fa fa-angle-right"></i>',
      sLast: '<i class="fa fa-angle-double-right"></i>',
    }
  }

  datatableInit(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      language: this.paginate,
      serverSide: true,
      responsive:false,
      searching:false,
      processing: true,
      order:[[0, 'desc']],
      scrollX:true,
      ajax: (dataTablesParameters: any, callback) => {
        this.http.PostAPI('admin/GetUsers',dataTablesParameters).then((resdata: any) => {
          if (resdata.status == 200) {
            this.getlist = resdata.response;
          } else {
            this.getlist = [];
          }
          callback({
            recordsTotal: resdata.TotalRecords['cnt'],
            recordsFiltered: resdata.TotalRecords['cnt'],
            data: []
          })
        }).catch((err) => {
          if (err.error == 'Unauthorized') {
            this.http.logout();
          }
          this.getlist = [];
          callback({
            recordsTotal: 0,
            recordsFiltered: 0,
            data: []
          })
        })
      },
      columns: [
        { data: 'fullname'},
        { data: 'email'},
        { data: 'mobile'},
        { data: 'audiofile'},
        { data: 'created_at'},
        { data: 'action',orderable:false}
      ]
    }
    setTimeout(() => {
      this.rerender()
    }, 1500)
  }

  rerender() {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      window.dispatchEvent(new Event('resize'));
    });
  }

  timer: any = "";
  filterById(e:any) {
    clearTimeout(this.timer)
    this.timer = setTimeout(() => {
      this.ReloadDatatable()
    }, 500)
  }

  ReloadDatatable() {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  get f() { return this.userForm.controls; }

  open_model(){
    this.title = 'Add';
    this.formValidation();
    this.show_model = true;
  }

  edit_model(id:any){
    this.previewaudiofile = ''
    this.http.PostAPI('admin/GetParticularUser', {user_id : id}).then((resdata: any) => {
      if (resdata.status == 200) {
        this.user_id = resdata.data.user_id
        this.previewaudiofile = this.backend_url + '' + resdata.data.audiofile
        this.userForm = this.formBuilder.group({
          fullname : [resdata.data.fullname,[Validators.required]],
          email : [resdata.data.email,[Validators.required, Validators.email]],
          mobile : [resdata.data.mobile,[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
          audiofile : ['']
        });
        this.title = 'Edit'
        this.show_model = true;
        this.ReloadDatatable()
      } else {
        this.toastr.error(resdata.message)
      }
    }).catch((err) => {
      this.toastr.error("Something went wrong");
    });
  }

  close_model(){
    this.show_model = false;
  }

  upload_audio(evt: any) {
    if (evt.target.files.length > 0) {
      this.audiofile = evt.target.files[0];
      var reader = new FileReader();
      reader.onload = (event: any) => {
        //this.previewaudiofile = event.target.result;
        //console.log(this.previewaudiofile)
      }
      reader.readAsDataURL(evt.target.files[0]);
    }
  }

  onSubmit(){
    this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }
    var data = this.userForm.value;
    var form_data  = new FormData()
    for (var key in data) {
      form_data.append(key, data[key]);
    }
    if(this.audiofile){
      form_data.append('audiofile', this.audiofile)
    }
    if(this.user_id != '' && this.user_id != null){
      form_data.append('user_id', this.user_id)
    }
    this.http.PostAPI('admin/addUser', form_data).then((resdata: any) => {
      if (resdata.status == 200) {
        this.toastr.success(resdata.message);
        this.show_model = false;
        this.ReloadDatatable()
      } else {
        this.toastr.error(resdata.message);
      }
    }).catch((err) => {
      this.toastr.error(err);
    });
  }

  view_model(id:any){
    this.previewaudiofile = ''
    this.http.PostAPI('admin/GetParticularUser', {user_id : id}).then((resdata: any) => {
      if (resdata.status == 200) {
        this.user_id = resdata.data.user_id
        this.fullname = resdata.data.fullname
        this.email = resdata.data.email
        this.mobile = resdata.data.mobile
        this.previewaudiofile = this.backend_url + '' + resdata.data.audiofile
        this.title = 'View'
        this.show_model1 = true;
      } else {
        this.toastr.error(resdata.message)
      }
    }).catch((err) => {
      this.toastr.error("Something went wrong");
    });
  }

  close_model1(){
    this.show_model1 = false
  }


}
