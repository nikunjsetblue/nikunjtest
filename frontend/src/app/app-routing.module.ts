import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminLoginComponent} from "./admin/admin-login/admin-login.component";
import {AdminUsersComponent} from "./admin/admin-users/admin-users.component";
import {RouterModule, Routes} from "@angular/router";
import {AuthGuardService} from "./service/auth-guard.service";
import {GuestGuardService} from "./service/guest-guard.service";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'prefix',
    component: AdminLoginComponent,
  },
  {
    path: 'admin',
    component: AdminLoginComponent,
    canActivate: [GuestGuardService],
  },
  {
    path: 'admin/login',
    component: AdminLoginComponent,
    canActivate: [GuestGuardService],
  },
  {
    path: 'admin/users',
    component: AdminUsersComponent,
    canActivate: [AuthGuardService],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
